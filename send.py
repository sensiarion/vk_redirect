import sqlite3
import re
import threading
import time
import requests
import config
from multiprocessing import Queue, Array
from sqlite_controller import Sqlite_controller as Db_controller


class Sender:

    def __init__(self, dbname):
        self.db = Db_controller(dbname)
        self.message_pool = Queue()
        self.thread_pool = []
        # NOT PROCESS SAVE
        self.error_pool = Queue()
        self.stop_pool = []

    @staticmethod
    def get_poop_url(server, key, ts):
        """
        Build url for access to server
        :param server:
        :param key:
        :param ts:
        :return:
        """
        return "http://%s?act=a_check&key=%s&ts=%s&wait=25&mode=74&version=2" % (
            server, key, ts)

    def get_pool_server(self, token, v):
        """
        Url for the first receive
        :param token: <str> Vk api token
        :param v: version of API (5.80 on this monet)
        :return: <str> Url to connect
        """
        url = "https://api.vk.com/method/messages.getLongPollServer?"
        try:
            response = requests.get(url + 'access_token=' + token + '&v=' + v)
        except TypeError:
            chat_id = self.db.user_find_by_token(token)
            self.db.user_update_param(chat_id, 'do_send', 0)
            self.error_pool.put(chat_id)
            return

        args = response.json()['response']
        pool_server_url = Sender.get_poop_url(args['server'], args['key'], args['ts'])

        return pool_server_url

    @staticmethod
    def get_new_pool(previous_pool, json_response):
        """
        for new vk_user's updates
        :param previous_pool: <url> last used url
        :param json_response: <json> last recieved response
        :return: <str> a new url for pool with new ts param
        """
        return re.sub('ts=\d+', 'ts=' + str(json_response['ts']), previous_pool)

    def pool_loop(self, user):
        """
        Initialize a loop for receive messages from 1 person and append it to message_lost
        :param user: <list> with params (vk_user_id,vk_api_token,telegram_chat_id)
        :param message_list: <list for append comming messages>
        :return:
        """
        pool_url = self.get_pool_server(user[2], '5.80')
        while True:
            try:
                contain_index = self.stop_pool.index(user[0])
                if contain_index >= 0:
                    self.stop_pool.pop(contain_index)
                    break
            except ValueError:
                pass
            response = requests.get(pool_url).json()
            try:
                pool_url = Sender.get_new_pool(pool_url, response)
            except:
                pool_url = self.get_pool_server(user[2], '5.80')
            for message in response['updates']:
                flags = bin(message[2])
                print('----------------')
                print(user)
                print(message)
                print('---------------')
                if message[0] == 4 and flags[-2] == '0':
                    self.message_pool.put({'vk_user_id': user[5],
                                           'from_id': message[3],
                                           'chat_id': user[0],
                                           'flags': bin(message[2]),
                                           'message': message[5]})
        print('Поток завршил работу' + str(user))

    def start_message_loop(self):
        """
        initialize message receiving (start a thread for each user)
            but only for accepted receiving before bot_start TODO fix it

        :return: None
        """
        print('Распределение потоков начато!')
        for user in self.db.get_users():
            if user[3] == 'ready' and user[4] == 1:
                print(user)
                t = threading.Thread(target=self.pool_loop, args=(user,), daemon=True)
                t.start()
                self.thread_pool.append(
                    [t, user[0]])

        # while True may be ?

    def apend_message_loop(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return:
        """
        check = False
        for appended_user in self.thread_pool:
            if appended_user[1] == chat_id:
                check = True
        print('Зашёл в добавление')
        if not check:
            print('Добавил!')
            user = self.db.get_user(chat_id)
            t = threading.Thread(target=self.pool_loop, args=(user,), daemon=True)
            t.start()
            self.thread_pool.append([t, user[0]])
            return True
        return False

    def pop_message_loop(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return: <list> (thread,chat_id)
        """
        print('Пытаюсь закрыть')
        for thr in self.thread_pool:
            print(self.thread_pool)
            if thr[1] == chat_id:
                print('Должен закрыть')
                self.stop_pool.append(chat_id)

    def start_send_loop(self, bot):
        """
        Initialize message sending loop (all messages for all users send from this loop)
        :param bot: <telebot.Telebot>
        :param message_pool: <list> list of tuple messages with keys: chat_id,message,from_id
        :return: None
        """
        print('Приём сообщений начат!')
        while True:
            try:
                message = self.message_pool.get()
                print('-----------------------------')
                print(message)
                chat_id = message['chat_id']
                content = message['message']

                users_get_url = "https://api.vk.com/method/users.get?access_token=%s&user_ids=%d&v='5.80" % (
                    config.Config.vk_service_token, message['from_id'])
                user_response = requests.get(users_get_url).json()
                print(user_response)
                if user_response['response']:
                    from_user = user_response['response'][0]['last_name'] + ' ' + user_response['response'][0][
                        'first_name']
                else:
                    print('Usage: start_send_loop\n Error via getting user name')
                    from_user = ''

                bot.send_message(chat_id, 'От ' + str(from_user) + ':\n' + content)
                time.sleep(0.3)
            except IndexError:
                print('нет сообщений')
                while not self.error_pool.empty():
                    chat_id = self.error_pool.get()
                    bot.send_message(chat_id,
                                     'Ваш токен неактивен, если вы хотите продолжить пользоваться моими услугами, пожалуйста, пропишите /start')
                time.sleep(2)
