import threading
import requests
import telebot
from config import Config
# Config contains: <str> tg token, <str> vk client id, <str> vk_service_token
import sqlite3
import hashlib
from sqlite_controller import Sqlite_controller as Controller
from send import Sender
import flask
from flask import request

queries = {
    'table_create': "CREATE TABLE IF NOT EXISTS users "
                    "(chat_id INTEGER PRIMARY KEY NOT NULL, "
                    "username VARCHAR(64), "
                    "token VARCHAR(128), "
                    "state VARCHAR(64) NOT NULL,"
                    "do_send BIT NOT NULL,"
                    "vk_user_id INTEGER)",

    'user_insert': "INSERT INTO users (chat_id,username,token,state,do_send) VALUES (?,?,?,?,?)",
    'user_update_username': "UPDATE users SET username = ? WHERE chat_id = ?",
    'user_update_state': "UPDATE users SET state = ? WHERE chat_id = ?",
    'user_update_do_send': "UPDATE users SET do_send = ? WHERE chat_id = ?",
    'user_update_token': "UPDATE users SET token = ? WHERE chat_id = ?",
    'user_update_token_and_vk_id': "UPDATE users SET token = ?, vk_user_id = ? WHERE chat_id = ?",
    'user_delete': "DELETE FROM users WHERE chat_id = ?",
    'user_get': "SELECT * FROM users WHERE chat_id = ?",
    'user_get_state': "SELECT state FROM users WHERE chat_id = ?",
    'user_get_do_send': "SELECT do_send FROM users WHERE chat_id = ?",
    'users_get': "SELECT * FROM users"
}

# TODO проверка работоспособности токена путём простого запроса

DBNAME = 'users.db'
scope = 'offline,friends,messages'


def token_keyboard(url):
    k = telebot.types.InlineKeyboardMarkup()
    k.add(telebot.types.InlineKeyboardButton(text='Получить токен', url=url))
    return k


def choose_keyboard():
    k = telebot.types.ReplyKeyboardMarkup(row_width=3)
    k.row(telebot.types.KeyboardButton('Да'), telebot.types.KeyboardButton('Нет'))
    return k


def exit_loop():
    word = ''
    while word != 'stop':
        word = input()
    exit(0)


def url_validate(url):
    """
    :param url: <str> url to vk token page
    :return: list of url's get params (token,user_id,state only)
    """
    if 'https://oauth.vk.com/blank.html#access_token=' in url:
        cuttted = url[url.find('token=') + 6:]
        token = cuttted[:cuttted.find('&')]
        temp = cuttted[cuttted.find('user_id=') + 8:]
        vk_user_id = temp[:temp.find('&')]
        state = cuttted[cuttted.find('state=') + 6:]

        return [token, vk_user_id, state]

    return False


def token_test(token):
    """
    Simple request to vk api service for validate token
    :param token:
    :return:
    """
    url = "https://api.vk.com/method/friends.get?access_token=%s&count=%d&v=5.80" % (token, 1)
    try:
        resp = requests.get(url)
        if resp.json()['response']:
            return True
    except:
        return False


def get_url(vk_api_client_id, scope, chat_id):
    """
    :param vk_api_client_id: <int> id of vk standalone app
    :param scope: <str> query of access to user's data
    :param chat_id: <int> telegram chat_id
    :return: <str> prepeared for give token query
    """
    url = "https://oauth.vk.com/authorize?client_id=%d&display=page&redirect_uri=blank.html" \
          "&scope=%s&response_type=token&v=5.80&state=%s" % (
              vk_api_client_id, scope, hash_sha(chat_id))
    return url


def hash_sha(input):
    """
    make a SHA256 hash of input
    :param input: <o> converts in a string by 'str()'
    :return: <hex> hexed hash
    """
    return hashlib.sha256(str(input).encode('UTF-8')).hexdigest()


# Инициализация переменной бота, сервера для приёма токенов и БД
bot = telebot.TeleBot(Config.token)
db = Controller(dbname=DBNAME)
sender = Sender('users.db')
app = flask.Flask(__name__)


@bot.message_handler(commands=['start'])
def start(message):
    url = get_url(Config.client_id, scope, message.chat.id)
    print('Ссылка на токен для ' + message.chat.username + '\n  ' + url)

    bot.send_message(message.chat.id,
                     'Привет, я бот, который позволяет тебе общаться с холопами контакта не выходя из любимого телеграма.'
                     ' Для работы тебе необходимо перейти по ссылке и отослать мне ту ссылку, на которую тебя перекинет',
                     reply_markup=token_keyboard(url))

    if db.check_user(message.chat.id) is False:
        db.create_user(message.chat.id, 'start', message.chat.username)
    else:
        db.user_state_update(message.chat.id, 'start')
        print('Пользователь уже в базе')


@bot.message_handler(commands=['stop'])
def stop(message):
    try:
        db.user_update_param(message.chat.id, 'do_send', 0)
    except TypeError:
        bot.send_message(message.chat.id, 'Я не нашёл тебя в базе, введи команду /start для начала взаимодействия')
        return
    sender.pop_message_loop(message.chat.id)
    bot.send_message(message.chat.id, 'Я отключил тебя, если хочешь возобновить, то пропиши /begin')


@bot.message_handler(commands=['delete'])
def delete(message):
    db.user_delete(message.chat.id)
    bot.send_message(message.chat.id,
                     'Я удалил все твои данные (токен и твой id вк, ведь сообщения я принципиально не храню)')


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id,
                     'Данный бот позволяет вам получать сообщения из контакта не выходя из любимой тележки\n'
                     'Для начала работы введите /start\n'
                     'Если хотите удалить все свои данные, то введите /delete\n'
                     'Если вы хотите остановить пересылку сообщений, то введите /stop\n'
                     'Для возобовления пересылки введите /begin')


@bot.message_handler(func=lambda message: message.text == 'Да' or message.text == '/begin', content_types=['text'])
def start(message):
    # TODO: check 'ready' state
    try:
        state = db.user_get_param(message.chat.id, 'state')
    except TypeError:
        bot.send_message(message.chat.id, 'Я не нашёл тебя в базе, введи команду /start для начала взаимодействия')
        return
    if state == 'ready':
        if token_test(db.user_get_param(message.chat.id, 'token')):
            db.user_update_param(message.chat.id, 'do_send', 1)
            sender.apend_message_loop(message.chat.id)
            bot.send_message(message.chat.id, 'Отлично! Если захочешь отменить, то просто пропиши /stop',
                             reply_markup=telebot.types.ReplyKeyboardRemove(False))
        else:
            bot.send_message(message.chat.id,
                             'Твой токен недействителен, будь так добр, введи команду /start и получите его заново')
    else:
        bot.send_message(message.chat.id,
                         'Что-то пошло не так (ты не прошёл аутентификацию), введи команду /start и получи токен заново')


@bot.message_handler(func=lambda message: message.text == 'Нет', content_types=['text'])
def start(message):
    try:
        db.user_update_param(message.chat.id, 'do_send', 0)
    except TypeError:
        bot.send_message(message.chat.id, 'Я не нашёл тебя в базе, введи команду /start для начала взаимодействия')
        return
    sender.pop_message_loop(message.chat.id)
    bot.send_message(message.chat.id, 'Ну и ладно', reply_markup=telebot.types.ReplyKeyboardRemove(False))


@bot.message_handler(content_types=['text'])
def text_handler(message):
    try:
        state = db.user_get_param(message.chat.id, 'state')
    except TypeError:
        bot.send_message(message.chat.id, 'Я не нашёл тебя в базе, введи команду /start для начала взаимодействия')
        return
    if state == 'start':
        params = url_validate(message.text)
        if params is False:
            bot.send_message(message.chat.id, 'На ссылку с токеном это совсем не похоже',
                             reply_markup=token_keyboard(get_url(Config.client_id, scope, message.chat.id)))
            return

        if params[2] != hash_sha(message.chat.id):
            bot.send_message(message.chat.id, 'Это не твоя ссылка',
                             reply_markup=token_keyboard(get_url(Config.client_id, scope, message.chat.id)))
            return

        if not token_test(params[0]):
            bot.send_message(message.chat.id,
                             'Не могу получить доступ по вашему токену, вы точно вернули мне нужную ссылку ?',
                             reply_markup=token_keyboard(get_url(Config.client_id, scope, message.chat.id)))
            return

        db.user_update_params(message.chat.id, ['token', 'vk_user_id', 'state'], [params[0], params[1], 'ready'])
        bot.send_message(message.chat.id, 'Начать пересылать сообщения ?', reply_markup=choose_keyboard())


@app.route('/')
def a():
    print(request.json)
    update = telebot.types.Update.de_json(request.get_json(force=True))
    return ''


@app.route('/' + Config.token, methods=['POST'])
def index():
    print(request.json)
    update = telebot.types.Update.de_json(request.get_json(force=True))

    return ''


if __name__ == '__main__':
    print('launching bot')
    # TODO: решить нужен ли внешний message_pool или вытаскивать его  из Sender'a
    # TODO: информирование о неправильном токене
    bot.remove_webhook()
    bot.set_webhook(url='https://%s:%s/%s' % (Config.webhook_ip, Config.webhook_port, Config.token),
                    certificate=open(Config.ssl_cert, 'r'))
    app.run(host='0.0.0.0', port=Config.webhook_port, ssl_context=(Config.ssl_cert, Config.ssl_pom))
    threading.Thread(target=sender.start_message_loop, args=(), daemon=True).start()
    threading.Thread(target=sender.start_send_loop, args=(bot,), daemon=True).start()
    # threading.Thread(target=bot.polling, kwargs={'none_stop': True, 'interval': 0}, daemon=True).start()
    exit_loop()
