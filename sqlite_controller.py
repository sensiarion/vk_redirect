import sqlite3


class Sqlite_controller:
    q = {
        'table_create': "CREATE TABLE IF NOT EXISTS users "
                        "(chat_id INTEGER PRIMARY KEY NOT NULL, "
                        "username VARCHAR(64), "
                        "token VARCHAR(128), "
                        "state VARCHAR(64) NOT NULL,"
                        "do_send BIT NOT NULL,"
                        "vk_user_id INTEGER)",
        'create_user': 'INSERT INTO users (chat_id,username,state,do_send) VALUES (?, ?, ?,0)',
        'get_user': 'SELECT * FROM users WHERE chat_id = ?'

    }

    def __init__(self, dbname):
        self.DBNAME = dbname
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(self.q['table_create'])
            connection.commit()

    def get_db_connection(self):
        """
        :param dbname: <str> name of database (like test.db)
        :return: <sqlite3.connection> object to communicate with db
        """
        print(self.DBNAME)
        return sqlite3.connect(self.DBNAME)

    def create_user(self, chat_id, state, username=None):
        """
        :param chat_id: <int> message.chat.id
        :param state: <str> user's state
        :param username: <str> nickname
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute(self.q['get_user'], (chat_id,))
            if cursor.fetchone() is None:
                connection.execute(self.q['create_user'], (chat_id, username, state))
                connection.commit()
                cursor.close()

    def check_user(self, chat_id):
        """
        check user's existing in DB
        :param chat_id: <int> message.chat.id
        :return: <boolean> True if exists, False if isn't
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute(self.q['get_user'], (chat_id,))
            if cursor.fetchone() is None:
                cursor.close()
                return False
            else:
                cursor.close()
                return True

    def get_user(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return: full user's info from DB
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute(self.q['get_user'], (chat_id,))
            response = cursor.fetchone()
            cursor.close()
            return response

    def user_state_update(self, chat_id, new_state):
        """
        update user's state in DB
        :param chat_id: <int> message.chat.id
        :param new_state: <str>
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute("UPDATE users SET state = ? WHERE chat_id = ?", (new_state, chat_id))
            connection.commit()

    def user_delete(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute("DELETE FROM users WHERE chat_id =?", (chat_id,))
            connection.commit()

    def user_find(self, username):
        """
        :param username: <str> person's tg username
        :return: <None/int> chat_id if exists, None else
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT chat_id FROM users WHERE username = ?", (username,))
            response = cursor.fetchone()[0]
            cursor.close()
            return response

    def user_find_by_token(self,token):
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT chat_id FROM users WHERE token = ?", (token,))
            response = cursor.fetchone()[0]
            cursor.close()
            return response


    def user_update_param(self, chat_id, param, value):
        """
        :param chat_id: <int> message.chat.id
        :param param: <str> user's table param (state,do_send,etc.)
        :param value: <>
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(("UPDATE users SET " + param + " = ? WHERE chat_id = ?"), (value, chat_id))
            connection.commit()

    def user_update_params(self, chat_id, params, values):
        """
        :param chat_id: <int> message.chat.id
        :param params: <list>
        :param values: <list>
        :return: None
        """
        s = "UPDATE users SET "
        if len(params) != len(params):
            raise KeyError
        if type(params) != list or type(values) != list:
            raise TypeError

        for i in range(len(params)):
            s += str(params[i]) + '=?,'
        s = s[:-1] + " WHERE chat_id = ?"
        print(s)

        print(s)
        values.append(chat_id)
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(s, values)
            connection.commit()

    def user_get_param(self, chat_id, param):
        """
        :param chat_id: <int> message.chat.id
        :param param: <str> name of param
        :return: <any> value of choosed param
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT " + param + " FROM users WHERE chat_id = ?", (chat_id,))
            response = cursor.fetchone()[0]
            cursor.close()
            return response

    def get_users(self):
        """
        :return: <tuple> all users from DB
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM users")
            return cursor.fetchall()
